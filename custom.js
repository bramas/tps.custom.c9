define(function(require, exports, module) {
    main.consumes = [
        "Plugin", "ui", "c9", "auth", "commands", "menus", "preferences", "settings", "tabManager", "dialog.notification", "fs"
    ];
    main.provides = ["tpscustom"];
    return main;

    function main(options, imports, register) {
        var Plugin = imports.Plugin;
        var ui = imports.ui;
        var menus = imports.menus;
        var commands = imports.commands;
        var settings = imports.settings;
        var prefs = imports.preferences;

        var tabs = imports.tabManager;
        var auth = imports.auth;
        var c9 = imports.c9;

        var _ = require("lodash");
        var basename = require("path").basename;

        /***** Initialization *****/

        var plugin = new Plugin("Ajax.org", main.consumes);
        var emit = plugin.getEmitter();

        var showing;
        function load() {

            settings.on("read", function(e){
                settings.setDefaults("user/tpscustom", [
                    ["username", ""]
                ]);
            });

            prefs.add({
                "TPS" : {
                    position: 4,
                    "Mes infos" : {
                        position: 100,
                        "username": {
                            type:"textbox",
                            setting: "user/tpscustom/@username",
                            position: 100
                        }
                    }
                }
            }, plugin);

            function setMenuCaption(item, caption) {
                // get item by path
                if (_.isString(item))
                    item = menus.get(item).item;

                // ensure item is object
                if (_.isObject(item))
                    item.setAttribute("caption", caption);
            }
            /**
             * Updates captions of some menus and menu items
             */
            function updateMenuCaptions() {
                // map paths to captions
                var captions = {
                    "Cloud9": "TPS IDE",
                };

                // update captions
                for (var path in captions)
                    setMenuCaption(path, captions[path]);
            }
            updateMenuCaptions();

            function updateTitle(tab) {
                var title = "TPS IDE";

                // append "Offline" to offline IDE title
                if (!c9.hosted)
                    title += " Offline";

                // prepend tab title when should
                document.title = tab && settings.getBool("user/tabs/@title")
                    && tab.title
                    ? tab.title + " - " + title
                    : title
            }
            function setTitleFromTabs() {
                // udpate document title initially
                updateTitle(tabs.focussedTab);

                // update document title when tab is focused
                tabs.on("focusSync", function(e) {
                    updateTitle(e.tab);
                }, plugin);

                // update document title when tab is destroyed
                tabs.on("tabDestroy", function(e) {
                    if (e.last)
                    updateTitle();
                }, plugin);

                // update document title when preference is toggled
                settings.on("user/tabs/@title", function() {
                    updateTitle(tabs.focussedTab);
                });
            }

            setTitleFromTabs();

            commands.addCommand({
                name:"previewFile",
                exec: function(editor, args) {
                    if(editor.length < 2) {
                        console.error("Indiquez un nom de fichier");
                        return;
                    }
                    var path = editor[1];
                    path = path.replace('/home/ubuntu/workspace', '');
                    commands.exec("preview", null, {
                        path: path
                    });
                }
            }, plugin);

            var request = new XMLHttpRequest();
            request.open('GET', 'https://api.tps.bramas.fr/api/lib-version', true);

            request.onload = function() {
              if (request.status >= 200 && request.status < 400) {
                // Success!
                //var data = JSON.parse(request.responseText);
                var versions = request.responseText.split('.');

                var fs = imports.fs;

                fs.readFile('~/.config/libtps_version', function(err, data) {
                    var localVersion = [0,0];
                    if(!err && data) {
                        localVersion = data.split('.');
                    } else {
                        data = '0.0';
                    }
                    if(parseInt(localVersion[0]) >= parseInt(versions[0]) && parseInt(localVersion[1]) >= parseInt(versions[1])){
                        return;
                    }

                    var notify = imports["dialog.notification"].show;

                    var showCloseButton = true;
                    notify("<div style='background-image: -webkit-linear-gradient(top, #ffb545 0%, #ffb545 100%);"+//height: 22px;background-color: #ffb545;text-align: center;font-size: 18px;font-weight: bold;color: #584100;
                    "' class='c9-readonly'>Tapez la commande 'tps-update' dans le terminal afin de mettre à jour les librairies ("+data+" -> "+request.responseText+")</div>", showCloseButton);
                })

              }
            };

            request.onerror = function() {
            };

            request.send();


        }


        /***** Lifecycle *****/

        plugin.on("load", function() {
            load();
        });
        plugin.on("unload", function() {
        });

        /***** Register and define API *****/

        /**
         * This is an example of an implementation of a plugin.
         * @singleton
         */
        plugin.freezePublicAPI({});

        register(null, {
            "tpscustom": plugin
        });
    }
});